var date = new Date();
var m = addZeroMin(date.getMinutes());
var currenttime = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear() + " - " + date.getHours() + ":" + m;
document.write("Current date and time is: " + currenttime);

function addZeroMin(min) {
    if (min < 10){
        min = "0" + min;
    }
    return min;
}
